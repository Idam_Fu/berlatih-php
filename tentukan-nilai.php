<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>tentukan nilai</title>
</head>
<body>
    <?php
    function tentukan_nilai($number)
    {
        // kode disini
        // lebih besar dari sama dengan 85 dan lebih kecil sama dengan 100 maka akan mereturn String “Sangat Baik” 
        // lebih besar sama dengan 70 dan lebih kecil dari 85 maka akan mereturn string “Baik”
        // lebih besar sama dengan 60 dan lebih kecil dari 70 maka akan mereturn string “Cukup” 
        // selain itu maka akan mereturn string “Kurang”
        if (($number >= 85) AND ($number<= 100)){
            echo "Sangat baik <br>";
        } else if (($number >= 70) AND ($number<= 85)){
            echo "Baik <br>";
        } else if (($number >= 60) AND ($number<= 70)){
            echo "Cukup <br>";
        } else {
            echo "Kurang <br>";
        }
    }
    
    //TEST CASES
    echo tentukan_nilai(98); //Sangat Baik
    echo tentukan_nilai(76); //Baik
    echo tentukan_nilai(67); //Cukup
    echo tentukan_nilai(43); //Kurang
    ?>
</body>
</html>